/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.util;

import java.io.File;
import java.io.PrintWriter;
import java.io.Writer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.SimpleLayout;
import org.savapage.common.SystemPropertyEnum;
import org.savapage.core.config.ConfigManager;
import org.savapage.core.config.RunModeEnum;
import org.savapage.core.dao.impl.DaoContextImpl;
import org.savapage.core.jpa.tools.DatabaseTypeEnum;
import org.savapage.core.jpa.tools.DbConfig;
import org.savapage.core.jpa.tools.DbProcessListener;
import org.savapage.core.jpa.tools.DbTools;
import org.savapage.core.services.ServiceContext;
import org.savapage.core.services.ServiceEntryPoint;

/**
 * Helper class to be used during build and packaging process.
 *
 * @author Rijk Ravestein
 *
 */
public final class App implements ServiceEntryPoint {

    // ........................................................
    // Commandline options
    // ........................................................
    /** */
    private static final String CLI_SWITCH_HELP = "h";
    /** */
    private static final String CLI_SWITCH_HELP_LONG = "help";
    /** */
    private static final String CLI_SWITCH_DBVERSION = "db-version";
    /** */
    private static final String CLI_SWITCH_DBINIT = "db-init";
    /** */
    private static final String CLI_SWITCH_DBCREATE = "db-create";
    /** */
    private static final String CLI_OPTION_DBTYPE = "db-type";
    /** */
    private static final String CLI_OPTION_DBIMPORT = "db-import";
    /** */
    private static final String CLI_OPTION_DBEXPORT = "db-export";
    /** */
    private static final String CLI_SWITCH_DBOPTIMIZE = "db-optimize";
    /** */
    private static final String CLI_OPTION_DBSQL_DROP = "db-sql-drop";
    /** */
    private static final String CLI_OPTION_DBSQL_CREATE = "db-sql-create";

    /** */
    private static final String CLI_OPTION_EXT_HIBERNATE_DIALECT =
            "db-ext-dialect";

    /** */
    private static final String CLI_OPTION_SERVER_HOME = "server-home";

    // ........................................................
    // Exit codes
    // ........................................................
    /** */
    public static final int EXIT_CODE_OK = 0;
    /** */
    public static final int EXIT_CODE_MISSING_PARMS = 1;
    /** */
    public static final int EXIT_CODE_PARMS_PARSE_ERROR = 2;
    /** */
    public static final int EXIT_CODE_EXCEPTION = 9;

    /**
     * The number of rows in the result set for export.
     */
    private static final int QUERY_MAX_RESULTS = 1000;

    /**
     * Shows usage and options of program on stdout.
     *
     * @param options
     *            The command line options.
     */
    public void usage(final org.apache.commons.cli.Options options) {
        org.apache.commons.cli.HelpFormatter formatter =
                new org.apache.commons.cli.HelpFormatter();
        formatter.printHelp("savapage-util <options>", options);
    }

    /**
     *
     * @return The {@link Options}.
     */
    private Options createCliOptions() {

        Options options = new Options();

        options.addOption(CLI_SWITCH_HELP, CLI_SWITCH_HELP_LONG, false,
                "Displays this help text.");

        options.addOption(
                Option.builder().hasArg(false).longOpt(CLI_SWITCH_DBVERSION)
                        .desc("Echoes db schema version to stdout.").build());

        options.addOption(
                Option.builder().hasArg(false).longOpt(CLI_SWITCH_DBINIT)
                        .desc("Initializes an existing database.").build());

        options.addOption(
                Option.builder().hasArg(false).longOpt(CLI_SWITCH_DBCREATE)
                        .desc("Creates a new database.").build());

        options.addOption(
                Option.builder().hasArg(false).longOpt(CLI_SWITCH_DBOPTIMIZE)
                        .desc("Optimizes the internal database.").build());

        options.addOption(Option.builder().hasArg(true).argName("file")
                .longOpt(CLI_OPTION_DBTYPE)
                .desc("Database type: \"Internal\" (default), " + "\""
                        + DatabaseTypeEnum.PostgreSQL.toString() + "\" or \""
                        + DatabaseTypeEnum.External.toString() + "\".")
                .build());

        options.addOption(Option.builder().hasArg(true).argName("class")
                .longOpt(CLI_OPTION_EXT_HIBERNATE_DIALECT)
                .desc("Hibernate Dialect class for \""
                        + DatabaseTypeEnum.External.toString() + "\" database.")
                .build());

        options.addOption(Option.builder().hasArg(true).argName("file|dir")
                .longOpt(CLI_OPTION_DBEXPORT)
                .desc("Exports the database to the given file or directory.")
                .build());

        options.addOption(Option.builder().hasArg(true).argName("file")
                .longOpt(CLI_OPTION_DBIMPORT)
                .desc("Imports the database from the given file. "
                        + "Deletes any existing data before loading the data.")
                .build());

        options.addOption(Option.builder().hasArg(true).argName("file")
                .longOpt(CLI_OPTION_DBSQL_CREATE)
                .desc("Writes sql script for creating the database to file.")
                .build());

        options.addOption(Option.builder().hasArg(true).argName("file")
                .longOpt(CLI_OPTION_DBSQL_DROP)
                .desc("Writes sql script for dropping the database to file.")
                .build());

        options.addOption(Option.builder().hasArg(true).argName("path")
                .longOpt(CLI_OPTION_SERVER_HOME)
                .desc("Overwrites the default \"server.home\" system property.")
                .build());

        return options;
    }

    /**
     *
     * @param args
     *            The arguments.
     * @return The exit code.
     * @throws ParseException
     *             When syntax error in command-line options.
     */
    private int run(final String[] args) throws ParseException {

        final Options options = createCliOptions();
        final CommandLineParser parser = new DefaultParser();
        final CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (org.apache.commons.cli.ParseException e) {
            System.out.println(e.getMessage());
            usage(options);
            return EXIT_CODE_PARMS_PARSE_ERROR;
        }

        if (cmd.getOptions().length == 0) {
            usage(options);
            return EXIT_CODE_MISSING_PARMS;
        }

        if (cmd.hasOption(CLI_SWITCH_HELP)
                || cmd.hasOption(CLI_SWITCH_HELP_LONG)) {
            usage(options);
            return EXIT_CODE_OK;
        }

        if (cmd.hasOption(CLI_SWITCH_DBVERSION)) {
            System.out.println(DbTools.getAppSchemaVersion());
            return EXIT_CODE_OK;
        }

        // ...................................................................
        // Check mandatory parameters
        // ...................................................................

        // ...................................................................
        // Validate CLI input
        // ...................................................................
        if (cmd.hasOption(CLI_OPTION_SERVER_HOME)) {
            SystemPropertyEnum.SAVAPAGE_SERVER_HOME
                    .setValue(cmd.getOptionValue(CLI_OPTION_SERVER_HOME));
        }

        // ...................................................................
        // Hardcoded default parameter values
        // ...................................................................
        ServiceContext.open();

        int ret = EXIT_CODE_EXCEPTION;

        try {

            /*
             * Set up a simple log4j configuration that logs on the console.
             */
            final ConsoleAppender appender = new ConsoleAppender();

            appender.setThreshold(Level.ERROR);
            appender.setName("myAppender");
            appender.setLayout(new SimpleLayout());

            final Writer writer = new PrintWriter(System.out);
            appender.setWriter(writer);

            BasicConfigurator.configure(appender);

            final DbProcessListener listener = new DbProcessListener() {

                @Override
                public void onLogEvent(final String message) {
                    System.out.println(message);
                }
            };

            /*
             * Get the database type.
             */
            DatabaseTypeEnum databaseType;

            try {
                if (cmd.hasOption(CLI_OPTION_DBTYPE)) {
                    databaseType = DatabaseTypeEnum
                            .valueOf(cmd.getOptionValue(CLI_OPTION_DBTYPE));
                } else {
                    databaseType = DatabaseTypeEnum.Internal;
                }
            } catch (IllegalArgumentException e) {
                databaseType = DatabaseTypeEnum.External;
            }

            if (databaseType == DatabaseTypeEnum.External) {
                if (cmd.hasOption(CLI_OPTION_EXT_HIBERNATE_DIALECT)) {
                    final DbConfig.HibernateInfo info =
                            new DbConfig.HibernateInfo();
                    info.setDialect(cmd
                            .getOptionValue(CLI_OPTION_EXT_HIBERNATE_DIALECT));
                    ConfigManager.putServerProps(info);
                }
            }

            listener.onLogEvent(
                    "Database [" + databaseType.getPropertiesId() + "] ...");

            if (cmd.hasOption(CLI_SWITCH_DBINIT)
                    || cmd.hasOption(CLI_SWITCH_DBCREATE)
                    || cmd.hasOption(CLI_OPTION_DBIMPORT)) {

                ConfigManager.instance().init(RunModeEnum.CORE, databaseType);
                DbTools.initDb(listener, cmd.hasOption(CLI_SWITCH_DBCREATE));

            } else if (cmd.hasOption(CLI_OPTION_DBSQL_CREATE)
                    || cmd.hasOption(CLI_OPTION_DBSQL_DROP)) {

                ConfigManager.instance().init(RunModeEnum.CORE, databaseType);
            } else {
                ConfigManager.instance().init(RunModeEnum.LIB, databaseType);
            }

            if (cmd.hasOption(CLI_OPTION_DBEXPORT)) {

                DbTools.exportDb(DaoContextImpl.peekEntityManager(),
                        QUERY_MAX_RESULTS,
                        new File(cmd.getOptionValue(CLI_OPTION_DBEXPORT)));
            }

            if (cmd.hasOption(CLI_OPTION_DBIMPORT)) {

                DbTools.importDb(
                        new File(cmd.getOptionValue(CLI_OPTION_DBIMPORT)),
                        listener);
            }

            if (cmd.hasOption(CLI_SWITCH_DBOPTIMIZE)) {
                DbTools.optimizeDbInternal();
            }

            if (cmd.hasOption(CLI_OPTION_DBSQL_CREATE)) {

                final File fileCreateSql =
                        new File(cmd.getOptionValue(CLI_OPTION_DBSQL_CREATE));

                final File fileDropSql =
                        new File(cmd.getOptionValue(CLI_OPTION_DBSQL_DROP));

                DbTools.generateDbSchema(databaseType, fileDropSql,
                        fileCreateSql);

                listener.onLogEvent(
                        "Created: " + fileCreateSql.getCanonicalPath());
                listener.onLogEvent(
                        "Created: " + fileDropSql.getCanonicalPath());
            }

            ret = EXIT_CODE_OK;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ServiceContext.close();
        }

        return ret;
    }

    /**
     *
     * @param args
     *            The arguments.
     */
    public static void main(final String[] args) {
        int status = EXIT_CODE_EXCEPTION;
        App app = new App();
        try {
            status = app.run(args);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.exit(status);
    }
}
